import React from 'react';
import styles from './HomeScreenStyle';
import {Text, View} from 'react-native';
import {Button} from '@Component';


export default function HomeScreen() {
  return (
    <View style = {styles.container}>
      <Button 
       title = {"Go Back"} 
      />
     </View>
  );
}
