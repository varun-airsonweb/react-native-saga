import { StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container : {
        alignItems: "center",
        backgroundColor: "#fff",
        paddingTop : 20,
    },    
    txtColor: {
        fontSize:40,
        fontWeight: "bold",
        color: "black", 
    },
    inputView: {
        backgroundColor: "lightgrey",
        borderRadius: 7,
        width: "90%",
        height: 40, 
        marginBottom: 20,   
     
    },
    TextInput: {
        marginLeft: 20,
        marginTop: 10,
        fontSize: 15,
        fontWeight: "bold",
      },
      btnStyle : {
        borderRadius:3,
        width : "100%",

      }
});
export default styles;

