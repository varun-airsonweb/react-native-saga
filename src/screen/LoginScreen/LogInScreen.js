import React,{useState} from 'react';
import {TouchableOpacity} from 'react-native'
import styles from './LoginScreenStyle';
import {View, } from 'react-native';
import {TextInput,Button,Text} from '@Component';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


export default function LogInScreen({navigation}) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // const [validation, setValidation] = useState(false);

  const loginValidation = () => {
  const reg =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let validation = false
  if (reg.test(email) == false){
    validation = false
    alert("Please Enter Valid Email");
}
if (email.length == 0) {
  validation = false
  // setPassword({ passwordErrorMessage: "Password is required feild"});
  alert("Email is required feild");
}
  if (password.length == 0) {
    validation = false
    // setPassword({ passwordErrorMessage: "Password is required feild"});
    alert("Password is required feild");
  }
   if (password.length < 8 || password.length > 20) {
    validation = false
    alert("Password should be min 8 char and max 20 char")
  }
  else {
    validation = true
}
  if(validation){
    navigation.navigate('Home')
  }
}

  return (
    <View style={styles.container}>
        <Text
        title={"Log In"}
        />

      <View style = {styles.inputView}>
      <TextInput 
      style = {styles.TextInput}
      placeholder= {"Email"}
      placeholderTextColor={"black"}
      onChangeText={(email) => setEmail(email)}
      />
      </View>

      <View style = {styles.inputView}>
      <TextInput 
      style = {styles.TextInput}
      placeholder= {"Password"}
      placeholderTextColor={"black"}
      secureTextEntry={true}
      onChangeText={(password) => setPassword(password)}
      />
      </View>
      {/* <TouchableOpacity  */}
      {/* // style = {styles.btnStyle}
       onPress={()=>{navigation.navigate('Home')}}> */}
      <Button 
      title={"Log In"}
      onPress={()=>{
        loginValidation();
      }}
      />
      {/* </TouchableOpacity> */}
     </View>
  );
}
