import { StyleSheet} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
	textFieldDefault: {
		fontSize:40,
        fontWeight: "bold",
        color: "black",
        marginBottom: 20,
	}

});
export default styles;

