import { StyleSheet} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
	textFieldDefault: {
		// height: 50,
		width: '100%',
		color:"black",
		fontSize: wp(2),
		alignItems:'center'
	}

});
export default styles;

