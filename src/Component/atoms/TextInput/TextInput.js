import React from 'react';
import {TextInput} from 'react-native';
import styles from './TextInputStyle';
const TextField = ({
  value,
  onSubmitEditing,
  placeholder,
  onChangeText,
  keyboardType,
  editable,
  onFocus,
  secureTextEntry,
  onBlur,
  autoFocus,
  length,
  placeholderTextColor,
  ...props
}) => (
  <TextInput
    maxLength={length}
    keyboardType={keyboardType ? keyboardType : 'default'}
    secureTextEntry={secureTextEntry ? secureTextEntry : false}
    style={styles.textFieldDefault}
    underlineColorAndroid={'transparent'}
    onChangeText={(text) => {
      onChangeText(text);
    }}
    value={value}
	  autoFocus={autoFocus}
    editable={editable}
    placeholder={placeholder}
    onSubmitEditing={() => onSubmitEditing}
    placeholderTextColor={placeholderTextColor}
    onFocus={() => {
      if (onFocus !== undefined) {
        onFocus();
      }
    }}
    onBlur={() => {
      if (onBlur !== undefined) {
        onBlur();
      }
    }}
    {...props}
  />
);

export default TextField;
