const INCREASE_COUNTER = 'INCREASE_COUNTER' ;
const DECREASE_COUNTER = 'DECREASE_COUNTER' ;
export function reduxIncreaseCounter(value) {
   return {
      type: INCREASE_COUNTER,
      payload: value,
   }
}
export function reduxDecreaseCounter(value) {
   return {
      type: DECREASE_COUNTER,
      payload: value,
   }
}
