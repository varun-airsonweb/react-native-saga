import React, { useEffect } from 'react';
import { SafeAreaView,BackHandler, StatusBar, StyleSheet,Text} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import Navigation from "./Navigation";
// import { setCurrentRouteName } from "./Store/global";

const Root = ({  }) => {
	const [bgBarColor, setBgBarColor] = React.useState('#fff');
	const [bgBarCotentColor, setBgBarCotentColor] = React.useState('dark-content');
	const [translucentMode, setTranslucentMode] = React.useState(false);


	// const effectDependency = stateGlobal.currentRouteName

	// useEffect(() => {
	// 	_StatusBar()

	// }, [effectDependency]);

	const _StatusBar = () => {
	//  if (stateGlobal.currentRouteName === 'Home'|| stateGlobal.currentRouteName === 'Detail'){
	// 		setBgBarColor('transparent')
	// 		setBgBarCotentColor('light-content')
	// 		setTranslucentMode(true)
	// 	}
	// 	else {
	// 		setBgBarColor('#fff')
	// 		setBgBarCotentColor('dark-content')
	// 		setTranslucentMode(false)
	// 	}
	}

	const backHandlerListener = (value) => {
		// if (stateGlobal.currentRouteName == 'Home') {
			
		// 	return true;
		// } else {
			BackHandler.removeEventListener("hardwareBackPress", backHandlerListener);
			return false;
		// }
	};
	useEffect(() => {
		BackHandler.addEventListener("hardwareBackPress", backHandlerListener);
		return () => {
			BackHandler.removeEventListener("hardwareBackPress", backHandlerListener);
		};
	}, [backHandlerListener])

	return (
		<>
			<SafeAreaView style={{ flex:1}}>
			
				<Navigation 
				// setCurrentRouteName={(value) => { dispatch(setCurrentRouteName(value)); }} 
				/>
		</SafeAreaView>


		</>
	);
}

export default Root;

const styles = StyleSheet.create({
	headerContainer: {
		flex: 1.4,
	}
})